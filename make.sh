# Creates some folders and files that are necessary to run the bot.

if [ $1 = ""]
then
	echo "No argument supplied."
	echo "Usage: make.sh <token>"
	exit
fi

# Making cache directory
mkdir cache/

# Filling cache
echo "[]" > cache/blacklist.json
echo "{}" > cache/customlevels.json
echo '{"identifies": [], "resumes": []}' > cache/debug.json
echo "{}" > cache/leveldata.json
echo "{}" > cache/tiledata.json

# Creating auth.json
echo -e "{\"token\": \"$1\"}" > config/auth.json

# Creating target/
mkdir target/

# Filling target/
mkdir -p target/letters/small
mkdir target/letters/big
mkdir target/letters/thick
mkdir -p target/render/custom

echo "Basic setup is done. It is highly recommended to issue 'loaddata' command on bot's first startup."